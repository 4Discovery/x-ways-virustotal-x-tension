## Description  

This X-Tension allows an examiner to check the status of a file via the VirusTotal API directly through X-Ways and get the status in the X-Ways messages window.

Note that this does **not** submit the file to VirusTotal, it only checks to see if an existing report exists for a given file�s hash and retrieves the results.  All checks are performed via SSL.

Requires Microsoft's .Net Framework v3.5 and a valid public (or private) API key from VirusTotal which can be obtained for free from [here](https://www.virustotal.com/en/documentation/public-api/).  

*Note the VirusTotal public API has a current limitation of 4 requests in any given 1 minute time frame, if you require more requests you can contact VirusTotal or apply for a private API key [here](https://www.virustotal.com/en/documentation/private-api/). See VirusTotal's API documentation for additional information.*

## Installation  

This X-Tension was developed and tested with X-Ways Forensic v17.7 but should work with any version past v16.9.

**Steps for Installation:**

- Log into [VirusTotal](https://www.virustotal.com/) and get your API key from the �My API key� link in the upper right under your account.  Your API key should be 65 characters in length.
- Depending on your use, download either the xtVirusTotal_32.dll or xtVirusTotal_64.dll and the sample xtVirusTotal.ini file from [here](https://bitbucket.org/4Discovery/x-ways-virustotal-x-tension/downloads).  
- Although this X-Tension will will work from any directory, it is recommended that you place the .dll and .ini file in the same directory as X-Ways Forensics (or in the \x64 subdirectory for 64-bit versions).  The .dll and .ini file should be kept together.
- Open the xtVirusTotal.ini file with your favorite text editor and paste in your VirusTotal API key as follows, replacing 11111.... with your VirusTotal API key.  

~~~~
    [Settings]    
    API_Key=11111111111111111111111111111111111111111111111111111111111111111
~~~~

## Usage  

This X-Tension is only available via the "Run X-Tensions..." right-click context menu from the X-Ways directory browser window and will only operate on one file at a time.  If more then one file is highlighted and sent to the X-Tension, only the first file will be processed.

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/xw_context_menu.png)

Click on the "+" and browse to the location of xtVirusTotal_32.dll or xtVirusTotal_64.dll depending on your version of X-Ways.

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/xw_xtension_empty.png)

Once the extension is loaded, you can edit or add your VirusTotal API key anytime from the about (...) button.  Changes will be saved in the xtVirusTotal.ini file in the same directory as the X-Tension.

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/xt_about.png)

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/xt_config.png)

If the file has never been scanned by VirusTotal, a "File is not found in VirusTotal" message will appear.

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/no_results.png)

If the file has been scanned, the messages window will provide you with the VirusTotal Scan ID, URL for the results, the date of the last scan, and the results for each of the anti-virus engines.

![screenshot](https://chadgough@bitbucket.org/4Discovery/x-ways-virustotal-x-tension/raw/master/img/results.png)


## Change Log  

- v1.0 (06-30-2014) - Initial Release

## References  

- Uses the X-Ways C# X-Tension API I published [here](https://github.com/chadgough/x-tensions) and documentation from the [X-Ways X-Tension API](http://www.x-ways.net/forensics/x-tensions/api.html)  
- Contains bits of code from [VirusTotal.NET](https://github.com/Genbox/VirusTotal.NET) and [RestSharp](http://restsharp.org)